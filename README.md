# INFORME ECS

## Amb Jobs

![](./Assets/img1.png)

Hem posat en els sistemes el schedule Parallel i la barrier per a utilitzar jobs.
Podem veure com tenim un valor de 45 FPS estable i de vegades pujava a uns 55 FPS, tenint un número d’entitats gaire alt.


## Sense Jobs

![](./Assets/img2.png)

En canvi aquí veiem com tenim 24 FPS tenint un número similar d’entitats.

