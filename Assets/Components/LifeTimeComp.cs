﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[GenerateAuthoringComponent]
public struct LifeTimeComponent : IComponentData
{
    public float TimeLeft;
    public float ActualTime;
}
