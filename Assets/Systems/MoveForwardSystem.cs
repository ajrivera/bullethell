﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class MoveForwardSystem : SystemBase
{
    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    protected override void OnUpdate()
    {
        var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();
        float deltaT = Time.DeltaTime;
        float elapsedT = (float)Time.ElapsedTime;

        Entities.WithAny<BulletTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, ref Rotation rot, in SpeedComponent speeds) =>
        {
            //float auxTime = (float)Time.ElapsedTime + entityInQueryIndex * math.PI;
            float auxTime = elapsedT + entityInQueryIndex * math.PI;
            //float4 auxrot += Quaternion.Euler()

            //rot.Value.y += speeds.AngularSpeed * Time.DeltaTime;

            //trans.Value += auxSpeed * Time.DeltaTime * math.forward(rot.Value);
            trans.Value += speeds.ForwardSpeed * deltaT * math.forward(rot.Value);

            //trans.Value += speeds.LateralSpeed * math.sin(auxTime * (math.PI * 2) * speeds.LateralFrequency) * deltaT * math.mul(Quaternion.Euler(0f, 90f, 0f), math.forward(rot.Value));

        }).ScheduleParallel();

        Entities.WithAny<RandomBulletTag>().ForEach((Entity entity, int entityInQueryIndex, ref Translation trans, ref Rotation rot, in SpeedComponent speeds) =>
        {
            //float auxTime = (float)Time.ElapsedTime + entityInQueryIndex * math.PI;
            float auxTime = elapsedT + entityInQueryIndex * math.PI;
            //float4 auxrot += Quaternion.Euler()

            float auxSpeed = speeds.ForwardSpeed + math.clamp(math.sin(entityInQueryIndex), 0, 2);
            //rot.Value.y += speeds.AngularSpeed * Time.DeltaTime;

            //trans.Value += auxSpeed * Time.DeltaTime * math.forward(rot.Value);
            trans.Value += auxSpeed * deltaT * math.forward(rot.Value);

            //trans.Value += speeds.LateralSpeed * math.sin(auxTime * (math.PI*2)*speeds.LateralFrequency) * Time.DeltaTime * math.mul(Quaternion.Euler(0f, 90f, 0f), math.forward(rot.Value));
            trans.Value += speeds.LateralSpeed * math.sin(auxTime * (math.PI * 2) * speeds.LateralFrequency) * deltaT * math.mul(Quaternion.Euler(0f, 90f, 0f), math.forward(rot.Value));

        }).ScheduleParallel();

    }
}
