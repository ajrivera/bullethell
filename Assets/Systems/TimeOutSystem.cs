﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

public class TimeOutSystem : SystemBase
{
    EntityCommandBufferSystem barrier => World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    protected override void OnUpdate()
    {
        var commandBuffer = barrier.CreateCommandBuffer().ToConcurrent();
        float3 PlayerPos = GameManager.playerpos;
        float deltaT = Time.DeltaTime;

        Entities.WithAny<BulletTag>().ForEach((Entity bullet, int entityInQueryIndex, ref LifeTimeComponent lifetime, in Translation trans) =>
        {
            
            PlayerPos.y = trans.Value.y;
            if (math.distance(PlayerPos, trans.Value) < 1f)
            {
                commandBuffer.DestroyEntity(entityInQueryIndex, bullet);
                
            }

            if (lifetime.ActualTime > lifetime.TimeLeft)
            {
                commandBuffer.DestroyEntity(entityInQueryIndex, bullet);
            }
            else
            {
                lifetime.ActualTime += deltaT;
            }

        }

        ).WithName("destroybulletjob").ScheduleParallel();

        Entities.WithAny<RandomBulletTag>().ForEach((Entity bullet, int entityInQueryIndex, ref LifeTimeComponent lifetime, in Translation trans) =>
        {

            PlayerPos.y = trans.Value.y;
            if (math.distance(PlayerPos, trans.Value) < 1f)
            {
                commandBuffer.DestroyEntity(entityInQueryIndex, bullet);

            }

            if (lifetime.ActualTime > lifetime.TimeLeft)
            {
                commandBuffer.DestroyEntity(entityInQueryIndex, bullet);
            }
            else
            {
                lifetime.ActualTime += deltaT;
            }

        }

        ).WithName("destroyrandombulletjob").ScheduleParallel();

        barrier.AddJobHandleForProducer(Dependency);
    }


}
