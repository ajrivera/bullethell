﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Jobs;
using Unity.Collections;
using Unity.Burst;
using UnityEngine.Jobs;
using Unity.Transforms;

using math = Unity.Mathematics.math;
using random = Unity.Mathematics.Random;

public class BulletManager : MonoBehaviour
{

    [Header("References")]
    public Transform canon;
    public float canonangle;
    private bool turn = false;
    private bool turn2 = false;
    public float turnspeed;
    private float auxTime;

    [Header("Spawn Settings")]
    public int nbullets;
    public float bulletspread;
    public float bulletspeed;
    public float bulletLspeed;
    public float bulletLfreq;
    public float bulletlifetime;


    private EntityManager entityManager;

    [SerializeField] private GameObject bulletprefab;
    [SerializeField] private GameObject randombulletprefab;
    private Entity bullet;
    private Entity randombullet;

    public float waitTime;
    //private JobHandle shootjobhandle;
    //private ShootJob kashootjob;

    private bool stopShoot = true;
    public bool debugmode;

    public enum ShootModes
    {
        mode1,
        mode2,
        paused
    }

    public ShootModes shootmode;

    // Start is called before the first frame update
    void Start()
    {
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;

        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, null);
        bullet = GameObjectConversionUtility.ConvertGameObjectHierarchy(bulletprefab, settings);
        randombullet = GameObjectConversionUtility.ConvertGameObjectHierarchy(randombulletprefab, settings);
        StartCoroutine(Shooting());
        //Shoot();
    }
    
    private IEnumerator Shooting()
    {
        float acc = 0;
        float acc2 = 0;
        int shootmodeacc = 0;
        bool debugbool = false;
        shootmode = ShootModes.mode1;
        while (stopShoot)
        {
            if (debugmode)
            {
                switch (shootmode)
                {
                    case ShootModes.mode1:
                        turn = true;
                        turn2 = false;
                        if (!debugbool)
                        {
                            auxTime = Time.time;
                            debugbool = true;
                        }
                        break;
                    case ShootModes.mode2:
                        turn = false;
                        turn2 = true;
                        debugbool = false;
                        break;
                    case ShootModes.paused:
                        turn = false;
                        turn2 = false;
                        debugbool = false;
                        break;
                }
            }
            else
            {

                print(acc + " " + acc2);
                if (acc2 > 0.4)
                {
                    shootmodeacc++;
                    if (shootmodeacc % 2 == 0)
                        shootmode = ShootModes.paused;
                    else
                    {
                        shootmode = (ShootModes)(int)Random.Range(0,2);
                    }
                    switch (shootmode){
                        case ShootModes.mode1:
                            turn = true;
                            turn2 = false;
                            auxTime = Time.time;
                            break;
                        case ShootModes.mode2:
                            turn = false;
                            turn2 = true;
                            break;
                        case ShootModes.paused:
                            turn = false;
                            turn2 = false;
                            break;
                    }
                    print("LETS TURN");
                    //if (turn2)
                    //{
                    //    if (turn)
                    //    {
                    //        turn = false;
                    //    }
                    //    else
                    //    {

                    //        turn = true;
                    //        auxTime = Time.time;
                    //    }
                    //}

                    acc2 = 0;
                }
            }

            if (acc > 0.2)
            {
                acc = 0;
                Shoot(true);
               // Shoot(false);
                print("RANDOMSHOOT");
            }
            else
            {
                Shoot(false);
                print("SHOOT");
            }

            yield return new WaitForSeconds(waitTime);
            acc += Time.deltaTime;
            acc2 += Time.deltaTime;

        }
    }

    private void Shoot(bool randomshoot)
    {
        float auxangle = bulletspread / nbullets;
        float startangle = (bulletspread / 2) * -1;

        float finalrot = canon.rotation.eulerAngles.y + startangle;
        NativeArray<Entity> bulletArray = new NativeArray<Entity>(nbullets, Allocator.Temp);
        for (int i = 0; i < nbullets; i++)
        {
            finalrot += auxangle;
            Quaternion aux = Quaternion.Euler(canon.rotation.eulerAngles.x, finalrot, canon.rotation.eulerAngles.z);

            if (!randomshoot)
                bulletArray[i] = entityManager.Instantiate(bullet);
            else
                bulletArray[i] = entityManager.Instantiate(randombullet);
            entityManager.SetComponentData(bulletArray[i], new Translation { Value = canon.position });
            entityManager.SetComponentData(bulletArray[i], new Rotation { Value = aux });
            entityManager.SetComponentData(bulletArray[i], new SpeedComponent { ForwardSpeed = bulletspeed, LateralSpeed = bulletLspeed, LateralFrequency = bulletLfreq });
            entityManager.SetComponentData(bulletArray[i], new LifeTimeComponent { TimeLeft = bulletlifetime, ActualTime = 0f });

        }

        bulletArray.Dispose();

    }


    //private struct ShootJob: IJobParallelFor
    //{
    //    public int nbullets;
    //    public float bulletspread;
    //    public float bulletspeed;
    //    public float bulletLspeed;
    //    public float bulletLfreq;
    //    public float bulletlifetime;
    //    public Transform canon;
    //    public Entity bullet;

    //    private NativeArray<Entity> bulletArray;

    //    public void Execute(int i)
    //    {

    //    }

    //}


    // Update is called once per frame
    void Update()
    {
        if (turn)
        {
            Quaternion rotaux = Quaternion.Euler(0f, turnspeed * (Time.time - auxTime)+90f,0f);

            canon.rotation = Quaternion.Lerp(canon.rotation, rotaux, .2f);
            print(Time.time - auxTime);
            //canon.Rotate(0f, turnspeed * Time.deltaTime, 0f);
        }
        else if(turn2){
            float t = (Mathf.Sin(Time.time) / 2) + .5f;

            Quaternion rot1 = Quaternion.Euler(0f, canonangle+90f, 0f);
            Quaternion rot2 = Quaternion.Euler(0f, -canonangle+90f, 0f);

            canon.rotation = Quaternion.Lerp(rot1, rot2, t);
        }
        else
        {
            canon.rotation = Quaternion.Lerp(canon.rotation, Quaternion.Euler(0f, 90f, 0f), .05f);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot(false);
            //kashootjob = new ShootJob()
            //{
            //    nbullets = this.nbullets,
            //    bulletspread = this.bulletspread,
            //    bulletspeed = this.bulletspeed,
            //    bulletLspeed = this.bulletLspeed,
            //    bulletLfreq = this.bulletLfreq,
            //    bulletlifetime = this.bulletlifetime,
            //    canon = this.canon,
            //    bullet = this.bullet
            //};
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (!stopShoot)
            {
                
                stopShoot = !stopShoot;
                StartCoroutine(Shooting());
            }
            else
            {
                stopShoot = !stopShoot;
                StopCoroutine(Shooting());
            }

            
        }
    }
}

